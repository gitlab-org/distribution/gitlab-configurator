# GitLab Configurator

## Overview

The GitLab configurator aims to improve the installation experience by
providing a way to walkthrough the configuration process. Administrators
will end up with a basic configuration that allows them to bootstrap GitLab
quickly in their target environment.

## Short Term Goals

The configurator must output configuration files and defining those is the
first step in the process. The first iteration conforms to the following
principles:

1. Deployment size does not have a large impact on the configuration file contents
1. Large deployments will be considered in the next iteration. Usage patterns are more important for those type of deployments are out of scope for a minimum viable change.
1. Defined Deployment Sizes
    - ***Small*** has 1-500 users
    - ***Medium*** has 100-4,999 users
    - ***Large*** has 5,000+ users
1. Configuration file output will be required and non-default configuration only with no comments; only a link to read about more options

## First Iteration Deliverables

### Common Deliverables

* Administrator Checklist
  * Number and Type of Nodes to provision
  * Memory sizing recommendations for each node type

### Small [ 1-500 users ]

* Single `gitlab.rb` file for a Single Node Installation

### Medium [ 1,000-4,999 users ]

* Primary `gitlab.rb`
    * Contains Information about Redis, PostgreSQL, and Gitaly Nodes
* Postgres Configuration
* Redis Configuration
* Application Node Configuration
    * Unicorn
    * Workhorse
    * Sidekiq
    * Pages
* Storage

### Large [ 5,000+ users ]

* Determined out of scope for miniumum viable change, development postponed to future iterations

## Long Term Goals

The GitLab Configurator aims to:

- notify administrators of changes in defaults or new features at upgrade
  time based on existing configuration
- allow the customization of documentation to environments providing a much
  more searchable and applicable administrator experience
- provide a pathway to orchestrated installation
- enable cost analysis based on configuration and provider targets to help
  with estimating deployment investments
