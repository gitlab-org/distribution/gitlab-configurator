# https://docs.gitlab.com/omnibus/settings/configuration.html
external_url 'https://gitlab.example.com'

# Use External Redis
redis['enable'] = false
gitlab_rails['redis_password'] = 'redispassword'
gitlab_rails['redis_host'] = 'redis.example.com'
gitlab_rails['redis_port'] = 6379

# Use External Postgres
postgresql['enable'] = false
gitlab_rails['db_adapter'] = 'postgresql'
gitlab_rails['db_encoding'] = 'utf8'
gitlab_rails['db_host'] = 'postgres.example.com'
gitlab_rails['db_port'] = 5432
gitlab_rails['db_username'] = 'gitlab'
gitlab_rails['db_password'] = 'password'

# External Gitaly
gitlab_rails['gitaly_token'] = 'abc123secret'
gitlab_shell['secret_token'] = 'itsasecretfromeverybody'
gitaly['enable'] = false

git_data_dirs({
  'default' => {
    'gitaly_address' => 'tls://gitaly.example.com:8076'
  }
})
