roles ['postgres_role']
repmgr['enable'] = false
consul['enable'] = false
postgresql['listen_address'] = '0.0.0.0'
postgresql['port'] = 5432
postgresql['sql_user_password'] = 'MD5_HASH_OF_PASSWORD'
postgresql['trust_auth_cidr_addresses'] = %w(127.0.0.1/32 CIDR_OF_APP_NODE)
gitlab_rails['auto_migrate'] = false
