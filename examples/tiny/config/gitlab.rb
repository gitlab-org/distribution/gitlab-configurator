# https://docs.gitlab.com/omnibus/settings/configuration.html
external_url 'https://gitlab.example.com'
unicorn['worker_processes'] = 2
sidekiq['concurrency'] = 9
prometheus_monitoring['enable'] = false
